unit gaugevectordouble;

{$mode objfpc}{$H+}

interface

uses gaugevectorinterface;

type
{ TGaugeRecordDoubleElement }
  TGaugeRecordDoubleElement = record
  name        : string;
  value       : double;
end;

  TGaugeRecordDoubleElements =  array of TGaugeRecordDoubleElement;
  PGaugeRecordDoubleElements = ^TGaugeRecordDoubleElements;

{ TGaugeRecordDouble }
  TGaugeVectorRecordDouble = record
    identificator : string;
    desciption    : string;
    device        : string;
    labelNames    : array of TGaugeRecordDoubleElement;
  end;

TClsGaugeVectorDouble = class (TInterfacedObject, TClsGaugeVectorInterface)
  var gaugeVecDoubleRec  : TGaugeVectorRecordDouble;
public
  constructor Create;
  destructor Destroy; override;
  procedure Increment(labelName : string; value: double);
  procedure SetValue(labelName : string; value : double);
  procedure SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
  function  GetValue(labelName : string) : double;
  function GetDesciption : string;
  function GetIdentificator : string;
  function GetDevice : string;
  function GetLabelNames : PGaugeRecordDoubleElements;
private
  procedure ChangeValue(labelName : string; value : double; replace : boolean);
end;

implementation

constructor TClsGaugeVectorDouble.Create;
begin
  gaugeVecDoubleRec.identificator := '';
  gaugeVecDoubleRec.desciption := '';
  SetLength(gaugeVecDoubleRec.labelNames,0);
end;


destructor TClsGaugeVectorDouble.Destroy;
begin
  inherited;
end;

procedure TClsGaugeVectorDouble.Increment(labelName : string; value: double);
begin
  ChangeValue(labelName, value, false);
end;

procedure TClsGaugeVectorDouble.SetValue(labelName : string; value: double);
begin
  ChangeValue(labelName, value, true);
end;

procedure TClsGaugeVectorDouble.SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
begin
  gaugeVecDoubleRec.identificator := valueIdentificator;
  gaugeVecDoubleRec.desciption := valueDescription;
  gaugeVecDoubleRec.device := valueDevice;
end;

procedure TClsGaugeVectorDouble.ChangeValue(labelName : string; value : double; replace : boolean);
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(gaugeVecDoubleRec.labelNames) do begin
            if(gaugeVecDoubleRec.labelNames[counterRec-1].name = labelName) then begin
               foundRec := true;
               if(replace)then begin
                  gaugeVecDoubleRec.labelNames[counterRec-1].value := value;
               end else begin
                  gaugeVecDoubleRec.labelNames[counterRec-1].value := gaugeVecDoubleRec.labelNames[counterRec-1].value + value;
               end;
               exit;
            end;
         end;
         if not(foundRec)then begin
           counterRec := length(gaugeVecDoubleRec.labelNames);
           counterRec := counterRec + 1;
           SetLength(gaugeVecDoubleRec.labelNames, counterRec);
           gaugeVecDoubleRec.labelNames[counterRec-1].name := labelName;
           gaugeVecDoubleRec.labelNames[counterRec-1].value := value;
         end;
end;

function TClsGaugeVectorDouble.GetValue(labelName : string) : double;
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(gaugeVecDoubleRec.labelNames) do begin
            if(gaugeVecDoubleRec.labelNames[counterRec-1].name = labelName) then begin
              Result := gaugeVecDoubleRec.labelNames[counterRec-1].value;
            end;
         end;
         if not (foundRec) then begin
            Result := -1.0;
         end;
end;
function TClsGaugeVectorDouble.GetDesciption : string;
begin
  Result := gaugeVecDoubleRec.desciption;
end;

function TClsGaugeVectorDouble.GetIdentificator : string;
begin
  Result := gaugeVecDoubleRec.identificator;
end;

function TClsGaugeVectorDouble.GetDevice : string;
begin
  Result := gaugeVecDoubleRec.device;
end;

function TClsGaugeVectorDouble.GetLabelNames : PGaugeRecordDoubleElements;
begin
  Result := @gaugeVecDoubleRec.labelNames;
end;

end.
