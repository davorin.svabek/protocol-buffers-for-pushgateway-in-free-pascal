unit counterdouble;

{$mode objfpc}{$H+}

interface

uses counterinterface;

  { TCounterRecordDouble }
type
  TCounterRecordDouble = record
    identificator : string;
    desciption    : string;
    value         : double;
  end;

TClsCounterDouble = class (TInterfacedObject, TClsCounterInterface)

  var counterDoubleRec  : TCounterRecordDouble;

public
  constructor Create;
  destructor Destroy; override;
  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
  procedure AddCounter(value : double);
  procedure SetCounter(value : double);
  procedure IncrementCounter;
  function  GetCounter : double;
  procedure ResetCounter;
  function  GetDesciption : string;
  function  GetIdentificator : string;
private
  procedure ChangeValue(value : double; replace : boolean);
  function GetValue : double;
end;

implementation

constructor TClsCounterDouble.Create;
begin
  counterDoubleRec.identificator := '';
  counterDoubleRec.desciption := '';
  counterDoubleRec.value := 0.0;
end;


destructor TClsCounterDouble.Destroy;
begin
  inherited;
end;

procedure TClsCounterDouble.AddCounter(value : double);
begin
  ChangeValue(value, false);
end;

procedure TClsCounterDouble.IncrementCounter;
begin
  ChangeValue(1.0, false);
end;

procedure TClsCounterDouble.SetCounter(value : double);
begin
  ChangeValue(value, true);
end;

procedure TClsCounterDouble.ResetCounter;
begin
  ChangeValue(0.0, true);
end;

function TClsCounterDouble.GetCounter : double;
begin
  Result := GetValue;
end;

procedure TClsCounterDouble.SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
begin
  counterDoubleRec.identificator := valueIdentificator;
  counterDoubleRec.desciption := valueDescription;
end;

procedure TClsCounterDouble.ChangeValue(value : double; replace : boolean);
begin
  if(replace) then begin
     counterDoubleRec.value := value;
  end else begin
     counterDoubleRec.value := counterDoubleRec.value + value;
  end;
end;

function TClsCounterDouble.GetValue : double;
begin
  Result := counterDoubleRec.value;
end;

function TClsCounterDouble.GetDesciption : string;
begin
  Result := counterDoubleRec.desciption;
end;

function TClsCounterDouble.GetIdentificator : string;
begin
  Result := counterDoubleRec.identificator;
end;

end.
