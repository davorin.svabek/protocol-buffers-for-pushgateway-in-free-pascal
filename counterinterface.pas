unit counterinterface;

{$mode objfpc}{$H+}

interface

type

  TClsCounterInterface = interface

  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription : string);
  function GetDesciption : string;
  function GetIdentificator : string;
end;

implementation

end.
