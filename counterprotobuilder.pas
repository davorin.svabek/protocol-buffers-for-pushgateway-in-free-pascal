unit counterprotobuilder;

{$mode objfpc}{$H+}

interface

uses Classes, counterinteger, countervectorinteger,  counterdouble, countervectordouble,
     protobufferhelper,(* pbPublic, pbInput, pbOutput,*) sysutils;

type
TClsCounterProtoBuilder = class
  const
  builderHeader                :string = chr(10);
  builderHeaderDelimiter       :string = chr(18);
  builderFooterNonVector       :string = chr(24) + chr(0) + chr(34) + chr(11) + chr(26) + chr(9) + chr(9);
  builderFooterVectorBegin     :string = chr(24) + chr(0);
  builderFooterVectorSegment   :string = chr(34);
  builderFooterVectorMarker    :string = chr(10) + chr(13) + chr(10);
  builderFooterVectorDelimiter :string = chr(18);
  builderFooterVectorEnd       :string = chr(26) + chr(9) + chr(9);

public
  function ToProtoBufferConverter(counterObject : TClsCounterDouble) : String; overload;
  function ToProtoBufferConverter(counterObject : TClsCounterInteger) : String; overload;
  function ToProtoBufferConverter(counterObject : TClsCounterVectorDouble): String; overload;
  function ToProtoBufferConverter(counterObject : TClsCounterVectorInteger): String; overload;
//  function ToProtoBufferLibraryConverter(counterObject : TClsCounterDouble) : String; overload;
//  function ToProtoBufferLibraryConverter(counterObject : TClsCounterInteger) : String; overload;
  function ToSubVectorProtoBufferConverter(device : string; counterElemets : PCounterRecordIntegerElements) : String; overload;
  function ToSubVectorProtoBufferConverter(device : string; counterElemets : PCounterRecordDoubleElements) : String; overload;

private

end;


implementation


function TClsCounterProtoBuilder.ToSubVectorProtoBufferConverter(device : string; counterElemets : PCounterRecordIntegerElements): String; overload;
var
    pseudoResult, subPseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    counterRecordIntegerElements :  array of TCounterRecordIntegerElement;
    counterRec : longint = 0;

begin

     pseudoResult := '';

     counterRecordIntegerElements := counterElemets^;

     sizeEncIdent := GetFormatedLengthOfString(device);

     for counterRec := 1 to length(counterRecordIntegerElements) do begin

        subPseudoResult :=  builderFooterVectorMarker + sizeEncIdent +  device;

        sizeEncDescrip := GetFormatedLengthOfString(counterRecordIntegerElements[counterRec-1].name);

        subPseudoResult := subPseudoResult + builderFooterVectorDelimiter + sizeEncDescrip +  counterRecordIntegerElements[counterRec-1].name;

        subPseudoResult := subPseudoResult +  builderFooterVectorEnd + IntegerToHexWithReverseOrder(counterRecordIntegerElements[counterRec-1].value);

        sizeEncFull := GetFormatedLengthOfString(subPseudoResult);

        pseudoResult := pseudoResult + builderFooterVectorSegment + sizeEncFull + subPseudoResult

     end;

     Result :=  pseudoResult;

end;

function TClsCounterProtoBuilder.ToSubVectorProtoBufferConverter(device : string; counterElemets : PCounterRecordDoubleElements): String; overload;
var
    pseudoResult, subPseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    counterRecordDoubleElements :  array of TCounterRecordDoubleElement;
    counterRec : longint = 0;

begin

     pseudoResult := '';

     counterRecordDoubleElements := counterElemets^;

     sizeEncIdent := GetFormatedLengthOfString(device);

     for counterRec := 1 to length(counterRecordDoubleElements) do begin

        subPseudoResult :=  builderFooterVectorMarker + sizeEncIdent +  device;

        sizeEncDescrip := GetFormatedLengthOfString(counterRecordDoubleElements[counterRec-1].name);

        subPseudoResult := subPseudoResult + builderFooterVectorDelimiter + sizeEncDescrip +  counterRecordDoubleElements[counterRec-1].name;

        subPseudoResult := subPseudoResult +  builderFooterVectorEnd + DoubleToHexWithReverseOrder(counterRecordDoubleElements[counterRec-1].value);

        sizeEncFull :=  GetFormatedLengthOfString(subPseudoResult);

        pseudoResult := pseudoResult + builderFooterVectorSegment + sizeEncFull + subPseudoResult

     end;

     Result :=  pseudoResult;

end;

function TClsCounterProtoBuilder.ToProtoBufferConverter(counterObject : TClsCounterInteger): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;

begin

     if (counterObject = nil) then begin
       Result := '';
       exit;
     end;

     sizeEncIdent := GetFormatedLengthOfString(counterObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(counterObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + counterObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + counterObject.GetDesciption +

               builderFooterNonVector + IntegerToHexWithReverseOrder(counterObject.GetCounter);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

function TClsCounterProtoBuilder.ToProtoBufferConverter(counterObject : TClsCounterVectorInteger): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    counterRecordIntegerElements : PCounterRecordIntegerElements;

begin

     if (counterObject = nil) then begin
       Result := '';
       exit;
     end;

     counterRecordIntegerElements := counterObject.GetLabelNames;

     sizeEncIdent := GetFormatedLengthOfString(counterObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(counterObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + counterObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + counterObject.GetDesciption +

               builderFooterVectorBegin + ToSubVectorProtoBufferConverter(counterObject.GetDevice, counterRecordIntegerElements);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

function TClsCounterProtoBuilder.ToProtoBufferConverter(counterObject : TClsCounterDouble): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;

begin

     if (counterObject = nil) then begin
       Result := '';
       exit;
     end;

     sizeEncIdent := GetFormatedLengthOfString(counterObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(counterObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + counterObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + counterObject.GetDesciption +

               builderFooterNonVector + DoubleToHexWithReverseOrder(counterObject.GetCounter);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;


function TClsCounterProtoBuilder.ToProtoBufferConverter(counterObject : TClsCounterVectorDouble): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    counterRecordDoubleElements : PCounterRecordDoubleElements;

begin

     if (counterObject = nil) then begin
       Result := '';
       exit;
     end;

     counterRecordDoubleElements := counterObject.GetLabelNames;

     sizeEncIdent := GetFormatedLengthOfString(counterObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(counterObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + counterObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + counterObject.GetDesciption +

               builderFooterVectorBegin + ToSubVectorProtoBufferConverter(counterObject.GetDevice, counterRecordDoubleElements);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

(*
function TClsCounterProtoBuilder.ToProtoBufferLibraryConverter(counterObject : TClsCounterInteger): String; overload;
     var
           FBuffer: TProtoBufOutput;
           Stream: TStream;
     begin
          FBuffer := TProtoBufOutput.Create;

          FBuffer.writeTag(0,73);
          FBuffer.writeString(1, counterObject.GetIdentificator);
          FBuffer.writeString(1, counterObject.GetDesciption);
          FBuffer.writeTag(0,24);
          FBuffer.writeTag(0,1);
          FBuffer.writeTag(0,34);
          FBuffer.writeTag(0,11);
          FBuffer.writeTag(0,18);
          FBuffer.writeTag(0,9);
          FBuffer.writeTag(0,9);
          FBuffer.writeInt64(0, counterObject.GetCounter);

          Stream := TMemoryStream.Create;
          Stream.Position:=0;
          FBuffer.SaveToStream(Stream);
          Stream.Position:=0;
          Result := Stream.ReadAnsiString;
end;

function TClsCounterProtoBuilder.ToProtoBufferLibraryConverter(counterObject : TClsCounterDouble): String; overload;
var
      FBuffer: TProtoBufOutput;
      Stream: TStream;
begin
     FBuffer := TProtoBufOutput.Create;

     FBuffer.writeTag(0,73);
     FBuffer.writeString(1, counterObject.GetIdentificator);
     FBuffer.writeString(1, counterObject.GetDesciption);
     FBuffer.writeTag(0,24);
     FBuffer.writeTag(0,1);
     FBuffer.writeTag(0,34);
     FBuffer.writeTag(0,11);
     FBuffer.writeTag(0,18);
     FBuffer.writeTag(0,9);
     FBuffer.writeTag(0,9);
     FBuffer.writeDouble(0, counterObject.GetCounter);

     Stream := TMemoryStream.Create;
     Stream.Position:=0;
     FBuffer.SaveToStream(Stream);
     Stream.Position:=0;
     Result := Stream.ReadAnsiString;
end;
*)
end.
