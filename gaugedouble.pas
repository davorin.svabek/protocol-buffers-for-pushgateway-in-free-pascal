unit gaugedouble;

interface

uses gaugeinterface;

  { TGaugeRecordDouble }
type
  TGaugeRecordDouble = record
    identificator : string;
    desciption    : string;
    value : double;
  end;

TClsGaugeDouble = class (TInterfacedObject, TClsGaugeInterface)

  var gaugeDoubleRec  : TGaugeRecordDouble;

public
  constructor Create;
  destructor Destroy; override;
  procedure Increment(value: double);
  procedure SetValue(value : double);
  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
  function  GetValue : double;
  function GetDesciption : string;
  function GetIdentificator : string;
private
  procedure ChangeValue(value : double; replace : boolean);
end;

implementation

constructor TClsGaugeDouble.Create;
begin
  gaugeDoubleRec.identificator := '';
  gaugeDoubleRec.desciption := '';
  gaugeDoubleRec.value := 0.0;
end;


destructor TClsGaugeDouble.Destroy;
begin
  inherited;
end;

procedure TClsGaugeDouble.Increment(value: double);
begin
  ChangeValue(value, false);
end;

procedure TClsGaugeDouble.SetValue(value: double);
begin
  ChangeValue(value, true);
end;

function TClsGaugeDouble.GetValue : double;
begin
  Result := gaugeDoubleRec.value;
end;

procedure TClsGaugeDouble.SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
begin
  gaugeDoubleRec.identificator := valueIdentificator;
  gaugeDoubleRec.desciption := valueDescription;
end;

procedure TClsGaugeDouble.ChangeValue(value: double; replace : boolean);
begin
      if(replace) then begin
         gaugeDoubleRec.value := value;
      end else begin
         gaugeDoubleRec.value := gaugeDoubleRec.value + value;
      end;
end;

function TClsGaugeDouble.GetDesciption : string;
begin
  Result := gaugeDoubleRec.desciption;
end;

function TClsGaugeDouble.GetIdentificator : string;
begin
  Result := gaugeDoubleRec.identificator;
end;

end.
