unit gaugevectorinteger;

{$mode objfpc}{$H+}

interface

uses gaugevectorinterface;

type
{ TGaugeRecordIntegerElement }
  TGaugeRecordIntegerElement = record
  name        : string;
  value       : longint;
end;

  TGaugeRecordIntegerElements =  array of TGaugeRecordIntegerElement;
  PGaugeRecordIntegerElements = ^TGaugeRecordIntegerElements;

{ TGaugeRecordInteger }
type
  TGaugeVectorRecordInteger = record
    identificator : string;
    desciption    : string;
    device        : string;
    labelNames    : array of TGaugeRecordIntegerElement;
  end;

TClsGaugeVectorInteger = class(TInterfacedObject, TClsGaugeVectorInterface)
  var gaugeVecIntegerRec : TGaugeVectorRecordInteger;
public
  constructor Create;
  procedure Increment(labelName : string; value: longint);
  procedure SetValue(labelName : string; value : longint);
  procedure SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
  function  GetValue(labelName : string) : longint;
  function GetDesciption : string;
  function GetIdentificator : string;
  function GetDevice : string;
  function GetLabelNames : PGaugeRecordIntegerElements;
private
  procedure ChangeValue(labelName : string; value : longint; replace : boolean);
end;

implementation

constructor TClsGaugeVectorInteger.Create;
begin
  gaugeVecIntegerRec.identificator := '';
  gaugeVecIntegerRec.desciption := '';
  SetLength(gaugeVecIntegerRec.labelNames,0);
end;


procedure TClsGaugeVectorInteger.Increment(labelName : string; value: longint);
begin
  ChangeValue(labelName, value, false);
end;

procedure TClsGaugeVectorInteger.SetValue(labelName : string; value: longint);
begin
  ChangeValue(labelName, value, true);
end;

procedure TClsGaugeVectorInteger.SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
begin
  gaugeVecIntegerRec.identificator := valueIdentificator;
  gaugeVecIntegerRec.desciption := valueDescription;
  gaugeVecIntegerRec.device := valueDevice;
end;

procedure TClsGaugeVectorInteger.ChangeValue(labelName : string; value : longint; replace : boolean);
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(gaugeVecIntegerRec.labelNames) do begin
            if(gaugeVecIntegerRec.labelNames[counterRec-1].name = labelName) then begin
               foundRec := true;
               if(replace)then begin
                  gaugeVecIntegerRec.labelNames[counterRec-1].value := value;
               end else begin
                  gaugeVecIntegerRec.labelNames[counterRec-1].value := gaugeVecIntegerRec.labelNames[counterRec-1].value + value;
               end;
               exit;
            end;
         end;
         if not(foundRec)then begin
           counterRec := length(gaugeVecIntegerRec.labelNames);
           counterRec := counterRec + 1;
           SetLength(gaugeVecIntegerRec.labelNames, counterRec);
           gaugeVecIntegerRec.labelNames[counterRec-1].name := labelName;
           gaugeVecIntegerRec.labelNames[counterRec-1].value := value;
         end;
end;

function TClsGaugeVectorInteger.GetValue(labelName : string) : longint;
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(gaugeVecIntegerRec.labelNames) do begin
            if(gaugeVecIntegerRec.labelNames[counterRec-1].name = labelName) then begin
              Result := gaugeVecIntegerRec.labelNames[counterRec-1].value;
            end;
         end;
         if not (foundRec) then begin
            Result := -1;
         end;
end;

function TClsGaugeVectorInteger.GetDesciption : string;
begin
  Result := gaugeVecIntegerRec.desciption;
end;

function TClsGaugeVectorInteger.GetIdentificator : string;
begin
  Result := gaugeVecIntegerRec.identificator;
end;

function TClsGaugeVectorInteger.GetDevice : string;
begin
  Result := gaugeVecIntegerRec.device;
end;

function TClsGaugeVectorInteger.GetLabelNames : PGaugeRecordIntegerElements;
begin
  Result := @gaugeVecIntegerRec.labelNames;
end;


end.
