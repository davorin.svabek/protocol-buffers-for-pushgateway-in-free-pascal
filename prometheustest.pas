program prometheustest;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Classes,
  Crt, SysUtils,
  fphttpclient,
  counterprotobuilder,
  gaugeprotobuilder,//, protobufferhelper,
  counterinteger,
  counterdouble,
  countervectorinteger,
  countervectordouble,
  gaugedouble,
  gaugeinteger,
  gaugevectordouble,
  gaugevectorinteger;

  function RandomRange(min, max: single): single;
  var
    double: single;
  begin
    double := Random;
    Result := min + double * (max - min);
  end;

var

  url, dataJ: string;
  ADouble: double;

  httpClient: TFPHTTPClient;
  Request: TStringStream;

  gaugedDouble: TClsGaugeDouble;
  gaugedInteger: TClsGaugeInteger;
  gaugedVecDouble: TClsGaugeVectorDouble;
  gaugedVecInteger: TClsGaugeVectorInteger;
  counteredDouble: TClsCounterDouble;
  counteredInteger: TClsCounterInteger;
  counteredVecDouble: TClsCounterVectorDouble;
  counteredVecInteger: TClsCounterVectorInteger;

  gaugedbuilder: TClsGaugeProtoBuilder;
  counterbuilder: TClsCounterProtoBuilder;

begin

  try
    // adresa pushgetway servera
    url := 'http://localhost:9091/metrics/job/my_job';

    // za http client
    httpClient := TFPHTTPClient.Create(nil);
    httpClient.KeepConnection := True;
    Request := TStringStream.Create('');
    httpClient.AddHeader('Accept-Encoding', 'gzip');
    httpClient.AddHeader('Content-Type', 'application/vnd.google.protobuf; proto=io.prometheus.client.MetricFamily; encoding=delimited');
    httpClient.AddHeader('User-Agent', 'Go-http-client/1.1');

    // za protobuffer protokol
    // > GAUGE (obican i vector)
    gaugedbuilder := TClsGaugeProtoBuilder.Create;
    gaugedDouble := TClsGaugeDouble.Create;
    gaugedInteger := TClsGaugeInteger.Create;
    gaugedVecDouble := TClsGaugeVectorDouble.Create;
    gaugedVecInteger := TClsGaugeVectorInteger.Create;
    // > COUNTER (obican i vector)
    counterbuilder := TClsCounterProtoBuilder.Create;
    counteredDouble := TClsCounterDouble.Create;
    counteredInteger := TClsCounterInteger.Create;
    counteredVecDouble := TClsCounterVectorDouble.Create;
    counteredVecInteger := TClsCounterVectorInteger.Create;

    while true do
    begin

      dataJ := '';

      //gaugedDouble.SetIdentificatorAndDesciption('gauge_double', 'Current temperature of the CPU.');
      //ADouble := RandomRange(10, 90 + 1);
      //gaugedDouble.SetValue(ADouble);
      //dataJ := dataJ + gaugedbuilder.ToProtoBufferConverter(gaugedDouble);

      gaugedDouble.SetIdentificatorAndDesciption('cpu_temperature_celsius', 'Current temperature of the CPU.');
      ADouble := RandomRange(10, 90 + 1);
      gaugedDouble.SetValue(ADouble);
      dataJ := dataJ + gaugedbuilder.ToProtoBufferConverter(gaugedDouble);

      gaugedInteger.SetIdentificatorAndDesciption('gauge_integer', 'Current temperature of the CPU.');
      ADouble := RandomRange(10, 90 + 1);
      gaugedInteger.SetValue(round(ADouble));
      dataJ := dataJ + gaugedbuilder.ToProtoBufferConverter(gaugedInteger);


      gaugedVecDouble.SetIdentificatorDesciptionDevice('gauge_double_vector', 'Current temperature of the CPU.', 'pc01');
      ADouble := RandomRange(10, 90 + 1);
      gaugedVecDouble.SetValue('cpu01', ADouble);
      ADouble := RandomRange(10, 90 + 1);
      gaugedVecDouble.SetValue('cpu02', ADouble);
      dataJ := dataJ + gaugedbuilder.ToProtoBufferConverter(gaugedVecDouble);

      gaugedVecInteger.SetIdentificatorDesciptionDevice('gauge_integer_vector', 'Current temperature of the CPU.', 'pc01');
      ADouble := RandomRange(10, 90 + 1);
      gaugedVecInteger.SetValue('cpu01', round(ADouble));
      ADouble := RandomRange(10, 90 + 1);
      gaugedVecInteger.SetValue('cpu02', round(ADouble));
      dataJ := dataJ + gaugedbuilder.ToProtoBufferConverter(gaugedVecInteger);


      counteredDouble.SetIdentificatorAndDesciption('cpu_temperature_double', 'Current temperature of the CPU.');
      ADouble := RandomRange(10, 90 + 1);
      counteredDouble.SetCounter(ADouble);
      dataJ := dataJ + counterbuilder.ToProtoBufferConverter(counteredDouble);

      //counteredInteger.SetIdentificatorAndDesciption('cpu_temperature_integer', 'Current temperature of the CPU.');
      //ADouble := RandomRange(10, 90 + 1);
      //counteredInteger.SetCounter(round(ADouble));
      //dataJ := dataJ + counterbuilder.ToProtoBufferConverter(counteredInteger);

      counteredInteger.SetIdentificatorAndDesciption('hd_errors_total', 'Current temperature of the CPU.');
      ADouble := RandomRange(10, 90 + 1);
      counteredInteger.AddCounter(round(ADouble));
      dataJ := dataJ + counterbuilder.ToProtoBufferConverter(counteredInteger);

      counteredVecDouble.SetIdentificatorDesciptionDevice('cpu_temperature_double_vector', 'Current temperature of the CPU.', 'pc01');
      ADouble := RandomRange(10, 90 + 1);
      counteredVecDouble.SetCounter('cpu01', ADouble);
      ADouble := RandomRange(10, 90 + 1);
      counteredVecDouble.SetCounter('cpu02', ADouble);
      dataJ := dataJ + counterbuilder.ToProtoBufferConverter(counteredVecDouble);

      counteredVecInteger.SetIdentificatorDesciptionDevice('cpu_temperature_integer_vector', 'Current temperature of the CPU.', 'pc01');
      ADouble := RandomRange(10, 90 + 1);
      counteredVecInteger.SetCounter('cpu01', round(ADouble));
      ADouble := RandomRange(10, 90 + 1);
      counteredVecInteger.SetCounter('cpu02', round(ADouble));
      dataJ := dataJ + counterbuilder.ToProtoBufferConverter(counteredVecInteger);

      Request.Position := 0;
      Request.WriteString(dataJ);

      Request.Position := 0;
      httpClient.RequestBody := Request;
      writeln(FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now) + ' ' + url);
      writeln(dataJ);
      httpClient.Put(url);
      Delay(1000);

      if KeyPressed then
        if ReadKey = ^C then
        begin
          writeln('Ctrl-C pressed');
          Break;
        end;
    end;

  finally

    FreeAndNil(httpClient);
    FreeAndNil(gaugedbuilder);
    FreeAndNil(gaugedDouble);
    FreeAndNil(gaugedInteger);
    FreeAndNil(gaugedVecDouble);
    FreeAndNil(gaugedVecInteger);
    FreeAndNil(counterbuilder);
    FreeAndNil(counteredDouble);
    FreeAndNil(counteredInteger);
    FreeAndNil(counteredVecDouble);
    FreeAndNil(counteredVecInteger);

  end;
end.
