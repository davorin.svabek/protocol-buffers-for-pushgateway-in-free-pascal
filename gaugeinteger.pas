unit gaugeinteger;

{$mode objfpc}{$H+}

interface

uses gaugeinterface;

  { TGaugeRecordInteger }
type
  TGaugeRecordInteger = record
    identificator : string;
    desciption    : string;
    value : longint;
  end;

TClsGaugeInteger = class(TInterfacedObject, TClsGaugeInterface)

  var gaugeIntegerRec : TGaugeRecordInteger;

public
  constructor Create;
  procedure Increment(value: longint);
  procedure SetValue(value : longint);
  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
  function  GetValue : longint;
  function GetDesciption : string;
  function GetIdentificator : string;
private
  procedure ChangeValue(value : longint; replace : boolean);
end;

implementation

constructor TClsGaugeInteger.Create;
begin
  gaugeIntegerRec.identificator := '';
  gaugeIntegerRec.desciption := '';
  gaugeIntegerRec.value := 0;
end;


procedure TClsGaugeInteger.Increment(value: longint);
begin
  ChangeValue(value, false);
end;

procedure TClsGaugeInteger.SetValue(value: longint);
begin
  ChangeValue(value, true);
end;

function TClsGaugeInteger.GetValue : longint;
begin
  Result := gaugeIntegerRec.value;
end;

procedure TClsGaugeInteger.SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
begin
  gaugeIntegerRec.identificator := valueIdentificator;
  gaugeIntegerRec.desciption := valueDescription;
end;

procedure TClsGaugeInteger.ChangeValue(value: longint; replace : boolean);
begin
      if(replace) then begin
         gaugeIntegerRec.value := value;
      end else begin
         gaugeIntegerRec.value := gaugeIntegerRec.value + value;
      end;
end;

function TClsGaugeInteger.GetDesciption : string;
begin
  Result := gaugeIntegerRec.desciption;
end;

function TClsGaugeInteger.GetIdentificator : string;
begin
  Result := gaugeIntegerRec.identificator;
end;

end.
