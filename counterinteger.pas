unit counterinteger;

interface

uses counterinterface;

  { TCounterRecordInteger }
type
  TCounterRecordInteger = record
    identificator : string;
    desciption    : string;
    value         : longint;
  end;

TClsCounterInteger = class (TInterfacedObject, TClsCounterInterface)

  var counterIntegerRec  : TCounterRecordInteger;

public
  constructor Create;
  destructor Destroy; override;
  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
  procedure AddCounter(value : longint);
  procedure SetCounter(value : longint);
  procedure IncrementCounter;
  function  GetCounter : longint;
  procedure ResetCounter;
  function  GetDesciption : string;
  function  GetIdentificator : string;
private
  procedure ChangeValue(value : longint; replace : boolean);
  function GetValue : longint;
end;

implementation

constructor TClsCounterInteger.Create;
begin
  counterIntegerRec.identificator := '';
  counterIntegerRec.desciption := '';
  counterIntegerRec.value := 0;
end;


destructor TClsCounterInteger.Destroy;
begin
  inherited;
end;

procedure TClsCounterInteger.AddCounter(value : longint);
begin
  ChangeValue(value, false);
end;

procedure TClsCounterInteger.IncrementCounter;
begin
  ChangeValue(1, false);
end;

procedure TClsCounterInteger.SetCounter(value : longint);
begin
  ChangeValue(value, true);
end;

procedure TClsCounterInteger.ResetCounter;
begin
  ChangeValue(0, true);
end;

function TClsCounterInteger.GetCounter : longint;
begin
  Result := GetValue;
end;

procedure TClsCounterInteger.SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
begin
  counterIntegerRec.identificator := valueIdentificator;
  counterIntegerRec.desciption := valueDescription;
end;

procedure TClsCounterInteger.ChangeValue(value : longint; replace : boolean);
begin
  if(replace) then begin
     counterIntegerRec.value := value;
  end else begin
     counterIntegerRec.value := counterIntegerRec.value + value;
  end;
end;

function TClsCounterInteger.GetValue : longint;
begin
  Result := counterIntegerRec.value;
end;

function TClsCounterInteger.GetDesciption : string;
begin
  Result := counterIntegerRec.desciption;
end;

function TClsCounterInteger.GetIdentificator : string;
begin
  Result := counterIntegerRec.identificator;
end;


end.
