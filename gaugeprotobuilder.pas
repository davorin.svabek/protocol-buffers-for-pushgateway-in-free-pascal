unit gaugeprotobuilder;

{$mode objfpc}{$H+}

interface

uses Classes, gaugeinteger, gaugevectorinteger, gaugedouble, gaugevectordouble,
     protobufferhelper, (*pbPublic, pbInput, pbOutput,*) sysutils;

type
TClsGaugeProtoBuilder = class
  const
  builderHeader                :string = chr(10);
  builderHeaderDelimiter       :string = chr(18);
  builderFooterNonVector       :string = chr(24) + chr(1) + chr(34) + chr(11) + chr(18) + chr(9) + chr(9);
  builderFooterVectorBegin     :string = chr(24) + chr(1);
  builderFooterVectorSegment   :string = chr(34);
  builderFooterVectorMarker    :string = chr(10) + chr(13) + chr(10);
  builderFooterVectorDelimiter :string = chr(18);
  builderFooterVectorEnd       :string = chr(18) + chr(9) + chr(9);

public
  function ToProtoBufferConverter(gaugeObject : TClsGaugeDouble) : String; overload;
  function ToProtoBufferConverter(gaugeObject : TClsGaugeInteger) : String; overload;
  function ToProtoBufferConverter(gaugeObject : TClsGaugeVectorDouble) : String; overload;
  function ToProtoBufferConverter(gaugeObject : TClsGaugeVectorInteger) : String; overload;
//  function ToProtoBufferLibraryConverter(gaugeObject : TClsGaugeDouble) : String; overload;
//  function ToProtoBufferLibraryConverter(gaugeObject : TClsGaugeInteger) : String; overload;
  function ToSubVectorProtoBufferConverter(device : string; gaugeElemets : PGaugeRecordIntegerElements) : String; overload;
  function ToSubVectorProtoBufferConverter(device : string; gaugeElemets : PGaugeRecordDoubleElements) : String; overload;

private

end;


implementation

function TClsGaugeProtoBuilder.ToSubVectorProtoBufferConverter(device : string; gaugeElemets : PGaugeRecordIntegerElements): String; overload;
var
    pseudoResult, subPseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    gaugeRecordIntegerElements :  array of TGaugeRecordIntegerElement;
    counterRec : longint = 0;

begin

     pseudoResult := '';

     gaugeRecordIntegerElements := gaugeElemets^;

     sizeEncIdent := GetFormatedLengthOfString(device);

     for counterRec := 1 to length(gaugeRecordIntegerElements) do begin

        subPseudoResult :=  builderFooterVectorMarker + sizeEncIdent +  device;

        sizeEncDescrip := GetFormatedLengthOfString(gaugeRecordIntegerElements[counterRec-1].name);

        subPseudoResult := subPseudoResult + builderFooterVectorDelimiter + sizeEncDescrip +  gaugeRecordIntegerElements[counterRec-1].name;

        subPseudoResult := subPseudoResult +  builderFooterVectorEnd + IntegerToHexWithReverseOrder(gaugeRecordIntegerElements[counterRec-1].value);

        sizeEncFull :=  GetFormatedLengthOfString(subPseudoResult);

        pseudoResult := pseudoResult + builderFooterVectorSegment + sizeEncFull + subPseudoResult

     end;

     Result :=  pseudoResult;

end;

function TClsGaugeProtoBuilder.ToSubVectorProtoBufferConverter(device : string; gaugeElemets : PGaugeRecordDoubleElements): String; overload;
var
    pseudoResult, subPseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    gaugeRecordDoubleElements :  array of TGaugeRecordDoubleElement;
    counterRec : longint = 0;

begin

     pseudoResult := '';

     gaugeRecordDoubleElements := gaugeElemets^;

     sizeEncIdent := GetFormatedLengthOfString(device);

     for counterRec := 1 to length(gaugeRecordDoubleElements) do begin

        subPseudoResult :=  builderFooterVectorMarker + sizeEncIdent +  device;

        sizeEncDescrip := GetFormatedLengthOfString(gaugeRecordDoubleElements[counterRec-1].name);

        subPseudoResult := subPseudoResult + builderFooterVectorDelimiter + sizeEncDescrip +  gaugeRecordDoubleElements[counterRec-1].name;

        subPseudoResult := subPseudoResult +  builderFooterVectorEnd + DoubleToHexWithReverseOrder(gaugeRecordDoubleElements[counterRec-1].value);

        sizeEncFull := GetFormatedLengthOfString(subPseudoResult);

        pseudoResult := pseudoResult + builderFooterVectorSegment + sizeEncFull + subPseudoResult

     end;

     Result :=  pseudoResult;

end;

function TClsGaugeProtoBuilder.ToProtoBufferConverter(gaugeObject : TClsGaugeInteger): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;

begin

     if (gaugeObject = nil) then begin
        Result := '';
        exit;
     end;

     sizeEncIdent := GetFormatedLengthOfString(gaugeObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(gaugeObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + gaugeObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + gaugeObject.GetDesciption +

               builderFooterNonVector + IntegerToHexWithReverseOrder(gaugeObject.GetValue);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

function TClsGaugeProtoBuilder.ToProtoBufferConverter(gaugeObject : TClsGaugeVectorInteger): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    gaugeRecordIntegerElements : PGaugeRecordIntegerElements;

begin

     if (gaugeObject = nil) then begin
        Result := '';
        exit;
     end;

     gaugeRecordIntegerElements := gaugeObject.GetLabelNames;

     sizeEncIdent := GetFormatedLengthOfString(gaugeObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(gaugeObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + gaugeObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + gaugeObject.GetDesciption +

               builderFooterVectorBegin + ToSubVectorProtoBufferConverter(gaugeObject.GetDevice, gaugeRecordIntegerElements);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

function TClsGaugeProtoBuilder.ToProtoBufferConverter(gaugeObject : TClsGaugeDouble): String; overload;
var
     pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;

begin

     if (gaugeObject = nil) then begin
        Result := '';
        exit;
     end;

     sizeEncIdent := GetFormatedLengthOfString(gaugeObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(gaugeObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + gaugeObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + gaugeObject.GetDesciption +

               builderFooterNonVector + DoubleToHexWithReverseOrder(gaugeObject.GetValue);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

function TClsGaugeProtoBuilder.ToProtoBufferConverter(gaugeObject : TClsGaugeVectorDouble): String; overload;
var
    pseudoResult, sizeEncIdent, sizeEncDescrip, sizeEncFull : string;
    gaugeRecordDoubleElements : PGaugeRecordDoubleElements;

begin

     if (gaugeObject = nil) then begin
        Result := '';
        exit;
     end;

     gaugeRecordDoubleElements := gaugeObject.GetLabelNames;

     sizeEncIdent := GetFormatedLengthOfString(gaugeObject.GetIdentificator);

     sizeEncDescrip := GetFormatedLengthOfString(gaugeObject.GetDesciption);

     pseudoResult := builderHeader + sizeEncIdent + gaugeObject.GetIdentificator  +

               builderHeaderDelimiter + sizeEncDescrip + gaugeObject.GetDesciption +

               builderFooterVectorBegin + ToSubVectorProtoBufferConverter(gaugeObject.GetDevice, gaugeRecordDoubleElements);

     sizeEncFull := GetFormatedLengthOfString(pseudoResult);

     pseudoResult := sizeEncFull + pseudoResult;

     Result := pseudoResult;
end;

(*
function TClsGaugeProtoBuilder.ToProtoBufferLibraryConverter(gaugeObject : TClsGaugeInteger): String; overload;
     var
           FBuffer: TProtoBufOutput;
           Stream: TStream;
     begin
          FBuffer := TProtoBufOutput.Create;

          FBuffer.writeTag(0,73);
          FBuffer.writeString(1, gaugeObject.GetIdentificator);
          FBuffer.writeString(1, gaugeObject.GetDesciption);
          FBuffer.writeTag(0,24);
          FBuffer.writeTag(0,1);
          FBuffer.writeTag(0,34);
          FBuffer.writeTag(0,11);
          FBuffer.writeTag(0,18);
          FBuffer.writeTag(0,9);
          FBuffer.writeTag(0,9);
          FBuffer.writeInt64(0, gaugeObject.GetValue);

          Stream := TMemoryStream.Create;
          Stream.Position:=0;
          FBuffer.SaveToStream(Stream);
          Stream.Position:=0;
          Result := Stream.ReadAnsiString;
end;

function TClsGaugeProtoBuilder.ToProtoBufferLibraryConverter(gaugeObject : TClsGaugeDouble): String; overload;
var
      FBuffer: TProtoBufOutput;
      Stream: TStream;
begin
     FBuffer := TProtoBufOutput.Create;

     FBuffer.writeTag(0,73);
     FBuffer.writeString(1, gaugeObject.GetIdentificator);
     FBuffer.writeString(1, gaugeObject.GetDesciption);
     FBuffer.writeTag(0,24);
     FBuffer.writeTag(0,1);
     FBuffer.writeTag(0,34);
     FBuffer.writeTag(0,11);
     FBuffer.writeTag(0,18);
     FBuffer.writeTag(0,9);
     FBuffer.writeTag(0,9);
     FBuffer.writeDouble(0, gaugeObject.GetValue);

     Stream := TMemoryStream.Create;
     Stream.Position:=0;
     FBuffer.SaveToStream(Stream);
     Stream.Position:=0;
     Result := Stream.ReadAnsiString;
end;
*)

end.
