unit gaugeinterface;

{$mode objfpc}{$H+}

interface

type

  TClsGaugeInterface = interface

  procedure SetIdentificatorAndDesciption(valueIdentificator: string; valueDescription: string);
  function GetDesciption : string;
  function GetIdentificator : string;
end;

implementation

end.
