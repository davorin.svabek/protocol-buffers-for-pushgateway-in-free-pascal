unit countervectorinteger;

{$mode objfpc}{$H+}

interface

uses countervectorinterface;


type
{ TCounterRecordIntegerElement }
  TCounterRecordIntegerElement = record
  name        : string;
  value       : longint;
end;

  TCounterRecordIntegerElements =  array of TCounterRecordIntegerElement;
  PCounterRecordIntegerElements = ^TCounterRecordIntegerElements;

{ TCounterVectorRecordInteger }
  TCounterVectorRecordInteger = record
    identificator : string;
    desciption    : string;
    device        : string;
    labelNames    : array of TCounterRecordIntegerElement;
  end;

TClsCounterVectorInteger = class (TInterfacedObject, TClsCounterVectorInterface)

  var counterVecIntegerRec  : TCounterVectorRecordInteger;

public
  constructor Create;
  destructor Destroy; override;
  procedure SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
  procedure AddCounter(labelName : string; value : longint);
  procedure SetCounter(labelName : string; value : longint);
  procedure IncrementCounter (labelName : string);
  function GetCounter(labelName : string) : longint;
  procedure ResetCounter(labelName : string);
  function GetDesciption : string;
  function GetIdentificator : string;
  function GetDevice : string;
  function GetLabelNames : PCounterRecordIntegerElements;
private
  procedure ChangeValue(labelName : string; value : longint; replace : boolean);
  function GetValue(labelName : string) : longint;
end;

implementation

constructor TClsCounterVectorInteger.Create;
begin
  counterVecIntegerRec.identificator := '';
  counterVecIntegerRec.desciption := '';
  SetLength(counterVecIntegerRec.labelNames,0);
end;


destructor TClsCounterVectorInteger.Destroy;
begin
  inherited;
end;

procedure TClsCounterVectorInteger.AddCounter(labelName : string; value : longint);
begin
  ChangeValue(labelName, value, false);
end;

procedure TClsCounterVectorInteger.IncrementCounter (labelName : string);
begin
  ChangeValue(labelName, 1, false);
end;

procedure TClsCounterVectorInteger.SetCounter(labelName : string; value : longint);
begin
  ChangeValue(labelName, value, true);
end;

procedure TClsCounterVectorInteger.ResetCounter(labelName : string);
begin
  ChangeValue(labelName, 0, true);
end;

function TClsCounterVectorInteger.GetCounter(labelName : string) : longint;
begin
  Result := GetValue(labelName);
end;

procedure TClsCounterVectorInteger.SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
begin
  counterVecIntegerRec.identificator := valueIdentificator;
  counterVecIntegerRec.desciption := valueDescription;
  counterVecIntegerRec.device := valueDevice;
end;

procedure TClsCounterVectorInteger.ChangeValue(labelName : string; value : longint; replace : boolean);
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(counterVecIntegerRec.labelNames) do begin
            if(counterVecIntegerRec.labelNames[counterRec-1].name = labelName) then begin
               foundRec := true;
               if(replace)then begin
                  counterVecIntegerRec.labelNames[counterRec-1].value := value;
               end else begin
                  counterVecIntegerRec.labelNames[counterRec-1].value := counterVecIntegerRec.labelNames[counterRec-1].value + value;
               end;
               exit;
            end;
         end;
         if not(foundRec)then begin
           counterRec := length(counterVecIntegerRec.labelNames);
           counterRec := counterRec + 1;
           SetLength(counterVecIntegerRec.labelNames, counterRec);
           counterVecIntegerRec.labelNames[counterRec-1].name := labelName;
           counterVecIntegerRec.labelNames[counterRec-1].value := value;
         end;
end;

function TClsCounterVectorInteger.GetValue(labelName : string) : longint;
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(counterVecIntegerRec.labelNames) do begin
            if(counterVecIntegerRec.labelNames[counterRec-1].name = labelName) then begin
              Result := counterVecIntegerRec.labelNames[counterRec-1].value;
            end;
         end;
         if not (foundRec) then begin
            Result := -1;
         end;
end;

function TClsCounterVectorInteger.GetDesciption : string;
begin
  Result := counterVecIntegerRec.desciption;
end;

function TClsCounterVectorInteger.GetIdentificator : string;
begin
  Result := counterVecIntegerRec.identificator;
end;

function TClsCounterVectorInteger.GetDevice : string;
begin
  Result := counterVecIntegerRec.device;
end;

function TClsCounterVectorInteger.GetLabelNames : PCounterRecordIntegerElements;
begin
  Result := @counterVecIntegerRec.labelNames;
end;

end.
