unit protobufferhelper;

{$mode objfpc}{$H+}

interface


uses
  Classes, SysUtils, StrUtils;

  function RandomRange(min,max:single):single;
  function HexToNibble(const hexvalue: char; out nibble: byte): boolean;inline;
  function HexToBin(HexValue, BinValue: PChar; BinBufSize: Integer): Integer;
  function HexToDouble(const s: string): double;
  function DoubleToHexWithReverseOrder(const value: double): string;
  function IntegerToHexWithReverseOrder(const value: longint): string;
  function IntegerToChar(const value: longint): string;
  function GetFormatedLengthOfString(const measuredString: string): string;

implementation

  function RandomRange(min,max:single):single;
  var
    double : single;
  begin
    double := Random;
    result := min + double* (max-min);
  end;

  function HexToNibble(const hexvalue: char; out nibble: byte): boolean;inline;
  begin
    if hexvalue IN ['0'..'9'] then
      nibble:=((ord(hexvalue)) and 15)
    else if char(byte(hexvalue) or $20) IN ['a'..'f'] then
      nibble:=((ord(hexvalue)+9) and 15)
    else
      Exit(False);
    Result := True;
  end;

  function HexToBin(HexValue, BinValue: PChar; BinBufSize: Integer): Integer;
  var
    i,j : integer;
    h,l : byte;

  begin
    i:=binbufsize;
    while (i>0) do
    begin
      if not HexToNibble(hexvalue^, h) then
        break;
      inc(hexvalue);

      if not HexToNibble(hexvalue^, l) then
        break;
      j := l + (h shl 4);
      inc(hexvalue);

      binvalue^:=chr(j);
      inc(binvalue);
      dec(i);
    end;
    result:=binbufsize-i;
  end;

  function HexToDouble(const s: string): double;
  type
    TNum = record
      case byte of
      1:(n32: DWord);
      2:(n64: QWord);
      3:(fs : Single);
      4:(fd : Double);
    end;

  var
    i: TNum;
  begin
    i.n64:=0;
    if s<>'' then HexToBin(@s[1],@i,sizeof(double));
    if Length(s)<9 then
    begin
      i.n32:=BEtoN(i.n32);
      HexToDouble:=i.fs;
    end
    else
    begin
      i.n64:=BEtoN(i.n64);
      HexToDouble:=i.fd;
    end;
  end;

  function DoubleToHexWithReverseOrder(const value: double): string;
  var
    wert: string;
    wert_byte: array[1..8] of string;
  begin

     wert:= IntToHex(PInt64(@value)^, 2);
     wert_byte[8]:= chr(Hex2Dec(copy(wert,0,2)));
     wert_byte[7]:= chr(Hex2Dec(copy(wert,3,2)));
     wert_byte[6]:= chr(Hex2Dec(copy(wert,5,2)));
     wert_byte[5]:= chr(Hex2Dec(copy(wert,7,2)));
     wert_byte[4]:= chr(Hex2Dec(copy(wert,9,2)));
     wert_byte[3]:= chr(Hex2Dec(copy(wert,11,2)));
     wert_byte[2]:= chr(Hex2Dec(copy(wert,13,2)));
     wert_byte[1]:= chr(Hex2Dec(copy(wert,15,2)));

     result := wert_byte[1] + wert_byte[2] + wert_byte[3] + wert_byte[4]
             + wert_byte[5]+ wert_byte[6] +wert_byte[7]+ wert_byte[8];
  end;

  function IntegerToHexWithReverseOrder(const value: longint): string;
  begin
     result := DoubleToHexWithReverseOrder(value);
  end;

  function IntegerToChar(const value: longint): string;
  var
    wert: string;
    wert_byte: array[1..2] of string;
    size : longint;
    pseudoResult : string;
  begin
     pseudoResult := '';
     wert:= IntToHex(value, 4);
     size := Length(wert);
     FillChar(wert_byte,SizeOf(wert_byte),0);
     if (SizeOf(wert_byte) > 0) then
        wert_byte[2]:= copy(wert,0,2);
     if (SizeOf(wert_byte) > 2) then
        wert_byte[1]:= copy(wert,3,2);

     if(value > 255) then begin

          pseudoResult :=  chr(strutils.Hex2Dec(wert_byte[2]));
     end;

     pseudoResult := pseudoResult + chr(strutils.Hex2Dec(wert_byte[1]));

     result := pseudoResult;

  end;

  function GetFormatedLengthOfString(const measuredString: string): string;
  var
    stringLengthCounter : longint;
    higherDigitCounter : longint = 0;
    const higherDigitShift : longint = 1;
    lowerDigitCounter :  longint = 0;
    const lowerDigitShift : longint = 128;
    encodedLength : string = '';

  begin
    stringLengthCounter := length(measuredString);
    if(stringLengthCounter >= lowerDigitShift) then
    begin
      higherDigitCounter := higherDigitShift + ((stringLengthCounter - lowerDigitShift) div lowerDigitShift);
      lowerDigitCounter := lowerDigitShift + (stringLengthCounter mod lowerDigitShift);
      encodedLength := IntegerToChar(lowerDigitCounter) + IntegerToChar(higherDigitCounter);
    end else begin
      encodedLength := IntegerToChar(stringLengthCounter);
    end;
    result := encodedLength;
  end;
end.

