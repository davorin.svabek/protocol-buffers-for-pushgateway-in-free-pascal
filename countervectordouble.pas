unit countervectordouble;

{$mode objfpc}{$H+}

interface

uses countervectorinterface;


type
{ TCounterRecordDoubleElement }
  TCounterRecordDoubleElement = record
  name        : string;
  value       : double;
end;

  TCounterRecordDoubleElements =  array of TCounterRecordDoubleElement;
  PCounterRecordDoubleElements = ^TCounterRecordDoubleElements;

  { TCounterVectorRecordDouble }
  TCounterVectorRecordDouble = record
    identificator : string;
    desciption    : string;
    device        : string;
    labelNames    : array of TCounterRecordDoubleElement;
  end;

TClsCounterVectorDouble = class (TInterfacedObject, TClsCounterVectorInterface)

  var counterVecDoubleRec  : TCounterVectorRecordDouble;

public
  constructor Create;
  destructor Destroy; override;
  procedure SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
  procedure AddCounter(labelName : string; value : double);
  procedure SetCounter(labelName : string; value : double);
  procedure IncrementCounter (labelName : string);
  function GetCounter(labelName : string) : double;
  procedure ResetCounter(labelName : string);
  function GetDesciption : string;
  function GetIdentificator : string;
  function GetDevice : string;
  function GetLabelNames : PCounterRecordDoubleElements;
private
  procedure ChangeValue(labelName : string; value : double; replace : boolean);
  function GetValue(labelName : string) : double;
end;

implementation

constructor TClsCounterVectorDouble.Create;
begin
  counterVecDoubleRec.identificator := '';
  counterVecDoubleRec.desciption := '';
  SetLength(counterVecDoubleRec.labelNames,0);
end;


destructor TClsCounterVectorDouble.Destroy;
begin
  inherited;
end;

procedure TClsCounterVectorDouble.AddCounter(labelName : string; value : double);
begin
  ChangeValue(labelName, value, false);
end;

procedure TClsCounterVectorDouble.IncrementCounter (labelName : string);
begin
  ChangeValue(labelName, 1.0, false);
end;

procedure TClsCounterVectorDouble.SetCounter(labelName : string; value : double);
begin
  ChangeValue(labelName, value, true);
end;

procedure TClsCounterVectorDouble.ResetCounter(labelName : string);
begin
  ChangeValue(labelName, 0.0, true);
end;

function TClsCounterVectorDouble.GetCounter(labelName : string) : double;
begin
  Result := GetValue(labelName);
end;

procedure TClsCounterVectorDouble.SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
begin
  counterVecDoubleRec.identificator := valueIdentificator;
  counterVecDoubleRec.desciption := valueDescription;
  counterVecDoubleRec.device := valueDevice;
end;

procedure TClsCounterVectorDouble.ChangeValue(labelName : string; value : double; replace : boolean);
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(counterVecDoubleRec.labelNames) do begin
            if(counterVecDoubleRec.labelNames[counterRec-1].name = labelName) then begin
               foundRec := true;
               if(replace)then begin
                  counterVecDoubleRec.labelNames[counterRec-1].value := value;
               end else begin
                  counterVecDoubleRec.labelNames[counterRec-1].value := counterVecDoubleRec.labelNames[counterRec-1].value + value;
               end;
               exit;
            end;
         end;
         if not(foundRec)then begin
           counterRec := length(counterVecDoubleRec.labelNames);
           counterRec := counterRec + 1;
           SetLength(counterVecDoubleRec.labelNames, counterRec);
           counterVecDoubleRec.labelNames[counterRec-1].name := labelName;
           counterVecDoubleRec.labelNames[counterRec-1].value := value;
         end;
end;

function TClsCounterVectorDouble.GetValue(labelName : string) : double;
var
    counterRec : longint;
    foundRec : boolean = false;
begin
         for counterRec := 1 to length(counterVecDoubleRec.labelNames) do begin
            if(counterVecDoubleRec.labelNames[counterRec-1].name = labelName) then begin
              Result := counterVecDoubleRec.labelNames[counterRec-1].value;
            end;
         end;
         if not (foundRec) then begin
            Result := -1.0;
         end;
end;

function TClsCounterVectorDouble.GetDesciption : string;
begin
  Result := counterVecDoubleRec.desciption;
end;

function TClsCounterVectorDouble.GetIdentificator : string;
begin
  Result := counterVecDoubleRec.identificator;
end;

function TClsCounterVectorDouble.GetDevice : string;
begin
  Result := counterVecDoubleRec.device;
end;

function TClsCounterVectorDouble.GetLabelNames : PCounterRecordDoubleElements;
begin
  Result := @counterVecDoubleRec.labelNames;
end;
end.
