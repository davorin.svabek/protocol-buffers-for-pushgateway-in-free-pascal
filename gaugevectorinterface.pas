unit gaugevectorinterface;

{$mode objfpc}{$H+}

interface

type

  TClsGaugeVectorInterface = interface

  procedure SetIdentificatorDesciptionDevice(valueIdentificator: string; valueDescription: string; valueDevice: string);
  function GetDesciption : string;
  function GetIdentificator : string;
  function GetDevice : string;
end;

implementation

end.
